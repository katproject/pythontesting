from locators_constants import LocatorsConstants
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

def test_login():
    options = Options()
    options.page_load_strategy = 'normal'
    driver = webdriver.Chrome(options=options)
    driver.get("https://admin-demo.nopcommerce.com/")
    # driver.find_element(By.XPATH, LocatorsConstants.USER_LOGIN_XPATH).send_keys('admin@yourstore.com')
    # driver.find_element(By.XPATH, LocatorsConstants.USER_PASSWORD_XPATH).send_keys('admin')
    driver.find_element(By.XPATH, LocatorsConstants.LOGIN_BUTTON_XPATH).click()
    # Define the element to be sure that we are at homepage
    user_name = driver.find_element(By.XPATH, LocatorsConstants.USER_NAME_XPATH)
    assert user_name.is_displayed()
    driver.quit()

test_login()
